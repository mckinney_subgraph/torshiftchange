# torshiftchange

torshiftchange is a utility to rotate Tor entry guards when the user changes
networks to prevent fingerprinting of the user based on a persistent set of
entry guards across different networks.

It is meant to address problems outlined here:
https://blog.torproject.org/category/tags/entry-guards

It is a slightly different implementation of:
https://github.com/leewoboo/tordyguards

## How it works

torshiftchange uses NetworkManager to determine the current set of networks the
user is connected to. It will query NetworkManager for the list of uuids for
each active connection and hash these values together to uniquely identify the
particular set of connections. This is necessary since the user may be
connected via multiple interfaces (Wifi, Wired, etc.). If the user disconnects
one of these interfaces, another unique state will be recorded.

The actual rotation is done by creating tor state files in /var/lib/tor with the
unique network state IDs appended to the file names. This allows us to keep a 
history of tor state in relation to previous network connections and then 
restoring them as needed. When connecting to a network, the script searches the
/var/lib/tor/ directory for a state file for that network. If one is found, 
the old state is restored. If no state file is found, a new one is created.

## Prequisites

(Debian)
sudo apt-get install python-dbus

## Install

1. Copy torshifthchange.py to /sbin
2. Copy 00torshiftchange to /etc/NetworkManager/dispatcher.d/



