#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import glob
import dbus
import hashlib
import logging
import pwd
import inspect
import shutil
import time

state_dir = '/var/lib/tor/'

class ShiftChange:

    def __init__(self):
        self.bus = dbus.SystemBus()
        self.logger = logging.getLogger('tor-shiftchange')
        fh = logging.FileHandler('/var/log/tor/shiftchange.log')
        fh.setLevel(logging.DEBUG)
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)
        self.logger.addHandler(fh)
        self.logger.addHandler(ch)

    def get_active_connections(self):
        uuids = []
        proxy = self.bus.get_object('org.freedesktop.NetworkManager', 
            '/org/freedesktop/NetworkManager')
        properties_manager = dbus.Interface(proxy, 
            'org.freedesktop.DBus.Properties')
        active_connections = properties_manager.Get(
            'org.freedesktop.NetworkManager', 'ActiveConnections')
        for ac in active_connections:
            ac_proxy = self.bus.get_object('org.freedesktop.NetworkManager',  ac)
            properties_manager = dbus.Interface(ac_proxy, 
                'org.freedesktop.DBus.Properties')
            uuid  = properties_manager.Get(
                'org.freedesktop.NetworkManager.Connection.Active', 'Uuid')
            uuids.append(uuid)
        return sorted(uuids)

    def hash_uuids(self, uuids):
        sha256 = hashlib.sha256()
        for uuid in uuids:
            sha256.update(uuid)
        digest = sha256.hexdigest()
        self.logger.warning("Hashing {0} to {1}".format(uuids, digest))
        return digest

    def compare_digests(self, uuids, digest):
        src_digest = self.ash_uuids(uuids)
        if src_digest == digest:
            return True
        return False

    def find_state_file(self, digest):
        return glob.glob(state_dir + "state." + digest)

    def down(self):
        os.remove = state_dir + "state"
        # TODO: Test success of file remove
        self.logger.warning("Running down() to clear tor state")

    def up(self): 
        uuids = self.get_active_connections()
        self.logger.warning("Running up() to restore tor state")
        digest = self.hash_uuids(uuids)
        src_state = state_dir + 'state.' + digest
        dst_state = state_dir + "state"
        if self.find_state_file(digest):
            # TODO: Test success of copyfile
            shutil.copy(old_state, new_state)
            self.logger.warning("Restored {0} to {1}".format(src_state, dst_state))

    def postup(self):
        uuids = self.get_active_connections()
        time.sleep(5) # Sleep while tor is restarting
        self.logger.warning("Running postup() to save tor state")
        digest = self.hash_uuids(uuids)
        src_state = state_dir + 'state'
        dst_state = state_dir + 'state.' + digest
        if os.path.isfile(src_state):
            shutil.copy(src_state, dst_state)
            self.logger.warning("Copied {0} to {1}".format(src_state, dst_state))
        else:
            # TODO: Implement retries
            self.logger.error("{0} does not exist".format(src_state))

if __name__ == "__main__":
    
    if pwd.getpwuid(os.getuid()).pw_name != 'debian-tor':
        print("Must be debian-tor")
        sys.exit(1)
    
    if len(sys.argv) < 2:
	print("Not enough arguments")
        sys.exit(1)
    if sys.argv[1] == 'up':
        sc = ShiftChange()
        sc.up()
    elif sys.argv[1] == 'down':
	sc = ShiftChange()
        sc.down()
    elif sys.argv[1] == 'postup':
        sc = ShiftChange()
        sc.postup()
    else:
        sys.exit(1)
